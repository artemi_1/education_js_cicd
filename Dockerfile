FROM node:12
WORKDIR /code
ADD . /code/
RUN npm install
EXPOSE 3000
CMD [ "node", "server.js" ]
